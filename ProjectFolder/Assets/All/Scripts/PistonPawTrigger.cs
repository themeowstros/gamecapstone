﻿using UnityEngine;
using System.Collections;

public class PistonPawTrigger : MonoBehaviour {
	Animator animator;
	Collider dummy;
	bool triggered;
	void Start()
	{
		animator = gameObject.GetComponent<Animator>();
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag.Equals("Player"))
		{
			triggered = true;
			dummy = other;
			animator.SetBool("Crushing",true);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag.Equals("Player"))
		{
			triggered = false;
			dummy = null;
			animator.SetBool("Crushing",false);
		}
	}

	void Update()
	{
		if(triggered)
		{
			if(dummy == null)
			{
				triggered = false;
				animator.SetBool("Crushing",false);
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class PlayerPhysicsV2 : MonoBehaviour {
	
	//How many horizontal or vertical rays to cast for collision detection
	public int hRays;
	public int vRays;

	//Collision states
	public bool grounded;
	public bool hitCeil;

	//What layers our player can collide with or phase-through
	public LayerMask geometryMask;
	public LayerMask phaseMask;

	//Collision detection variables
	private BoxCollider activeCollider;
	private BoxCollider runningCollider;
	private BoxCollider slidingCollider;
	private Vector3 size;
	private Vector3 center;
	private float skin = .05f;

	void Start()
	{
		BoxCollider[] colliders = GetComponents<BoxCollider>();
		runningCollider = colliders[0];
		slidingCollider = colliders[1];
		activeCollider = runningCollider;
		size = activeCollider.size;
		center = activeCollider.center;
	}

	//Move the player
	public bool Move(Vector3 move, bool playerPhased, bool playerSliding)
	{
		SetCorrectColliderForSlidingState(playerSliding);

		Vector3 pos = transform.position;
		Vector3 scale = transform.localScale;
		size = Vector3.Scale(activeCollider.size, scale);
		center = Vector3.Scale(activeCollider.center, scale);

		float deltaX = move.x;
		float deltaY = move.y;
		float dirX = Mathf.Sign(deltaX);
		float dirY = Mathf.Sign(deltaY);

		Ray ray;
		Vector3 origin;
		origin.z = pos.z + center.z;
		Vector3 hDir = new Vector3(1,0,0);
		Vector3 vDir = new Vector3(0,dirY,0);

		//Default the collision flags
		grounded = false;
		hitCeil = false;
		LayerMask collisionMask = geometryMask;

		if (!playerPhased) {
			collisionMask = phaseMask + collisionMask;
		}

		bool hit = false;
		//Check for horizontal collisions
		for(int i = 0; i < hRays; i++)
		{
			float dist;
			origin.x = pos.x + center.x + (size.x/2) * dirX;
			origin.y = pos.y + center.y + (size.y/2) - (size.y/(hRays-1)) * i;
			origin.z = pos.z;
			ray = new Ray(origin,hDir);
			dist = CollisionCheck(ray,deltaX,collisionMask);
			if(dist<deltaX)
			{
				deltaX = dist;
				hit = true;
			}
			Debug.DrawRay(ray.origin,ray.direction);
		}

		if (hit) {
			return false;
		}

		//Check for vertical collisions
		for(int i = 0; i < vRays; i++)
		{
			origin.x = pos.x + center.x - (size.x/2) + (size.x/(vRays-1)) * i;
			origin.y = pos.y + center.y + (size.y/2) * dirY;
			origin.z = pos.z;
			ray = new Ray(origin,vDir);
			float dist = CollisionCheck (ray, Mathf.Abs(deltaY),collisionMask);
			if(dist<Mathf.Abs(deltaY))
			{
				deltaY = dist * dirY;
				if(dirY<1) //If we are falling and we collide with something we become grounded
					grounded = true;
				else 		//Else we are rising and have hit the ceiling
					hitCeil = true;
			}
			Debug.DrawRay (ray.origin,ray.direction);
		}

		move.x = deltaX;
		move.y = deltaY;
		transform.Translate(move);
		return true;
	}

	private void SetCorrectColliderForSlidingState(bool playerSliding) {
		activeCollider = playerSliding ? slidingCollider : runningCollider;
	}

	private float CollisionCheck(Ray ray, float delta, LayerMask checkMask)
	{
		if(delta<=0)
			return 0;
		RaycastHit hit;
		bool result = Physics.Raycast(ray.origin,ray.direction,out hit,delta,checkMask);
		if(result)
			delta = hit.distance - skin;
		else
		if(delta<skin)
		{
			result = Physics.Raycast(ray.origin,ray.direction,out hit,skin,checkMask);
			if(result)
				delta = 0;
		}

		return delta;
	}
}

﻿using UnityEngine;
using System.Collections;

public class TutorialRKunCaller : MonoBehaviour {

	public GameObject Rkun;
	public float life;
	private float start;

	// Use this for initialization
	void Start () 
	{
		Rkun.SetActive (false);
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other) 
		{

		if (other.tag.Equals ("Player")) 
			{
				Rkun.SetActive (true);
				start = Time.time;
			}
		}

	void FixedUpdate ()
		{

		if (Time.time - start > life) 
			{
				Rkun.SetActive (false);
			}
		}

}
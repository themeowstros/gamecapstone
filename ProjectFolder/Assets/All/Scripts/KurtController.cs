﻿using UnityEngine;
using System.Collections;

public class KurtController : MonoBehaviour {
	
	public float speed;
	public float jumpSpeed;
	public Transform leftGroundCheck;
	public Transform rightGroundCheck;
	public float groundCheckDistance;
	public float jumpApexVelocityEpsilon;
	public float floatForce;
	
	bool grounded = false;
	bool jumpHeld = false;
	public LayerMask ground;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 point1 = new Vector3(leftGroundCheck.position.x, leftGroundCheck.position.y + groundCheckDistance, leftGroundCheck.position.z);
		Vector3 point2 = new Vector3(rightGroundCheck.position.x, rightGroundCheck.position.y - groundCheckDistance, rightGroundCheck.position.z);
		grounded = Physics.Linecast (point1, point2, ground);

		Debug.Log (grounded);
		// Physics stuff
		float move = Input.GetAxisRaw ("Horizontal");
		
		if (!grounded && rigidbody.velocity.y < jumpApexVelocityEpsilon) {
			if (!grounded && jumpHeld) {
				rigidbody.AddForce(Vector3.up * floatForce);
			}
		}
		
		rigidbody.velocity = new Vector3 (move * speed, rigidbody.velocity.y, 0.0f);
	}
	
	void Update () {
		if (grounded && !jumpHeld && Input.GetAxis("Jump") > 0) {
			rigidbody.velocity = new Vector3 (rigidbody.velocity.x, jumpSpeed, 0.0f);
			jumpHeld = true;
		}
		if (!(Input.GetAxis ("Jump") > 0)) {
			jumpHeld = false;
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class DiscoLight : MonoBehaviour {
	Color color;
	public float changeTime;
	private float currentTime;

	// Use this for initialization
	void Start () {
		Color color = light.color;
		currentTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(currentTime >= changeTime)
		{
			color = new Color(Random.value,Random.value,Random.value);
			currentTime = 0;
		}
		currentTime+=Time.deltaTime;
		light.color = Color.Lerp(light.color,color,currentTime/changeTime);
	}
}

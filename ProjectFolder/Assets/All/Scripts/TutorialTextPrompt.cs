﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialTextPrompt : MonoBehaviour {
	Text text;
	public string prompt;

	// Use this for initialization
	void Start () {
		text = FindObjectOfType<Text>();
	}
	
	// Player hits the trigger
	void OnTriggerEnter(Collider other)
	{
		if(other.tag.Equals("Player"))
			text.text = prompt;
	}
}

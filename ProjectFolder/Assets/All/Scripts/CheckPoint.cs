﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {

	GameController gameController;

	void Start()
	{
		gameController = Object.FindObjectOfType<GameController>();
	}

	void OnTriggerEnter(Collider other)
	{
		gameController.CheckPoint(transform.position);
	}
}

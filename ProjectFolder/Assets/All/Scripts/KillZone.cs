﻿using UnityEngine;
using System.Collections;

public class KillZone : MonoBehaviour {

	GameController gameController;

	void Start()
	{
		gameController = Object.FindObjectOfType<GameController>();
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag.Equals("Player"))
			gameController.RespawnPlayer();
	}
}

﻿using UnityEngine;
using System.Collections;

public class ProjectileCaller : MonoBehaviour {

	public GameObject Projectile;
	public Transform SpawnPoint;

	void OnTriggerEnter ( Collider other ) {

		if (other.gameObject.tag == "Player") {

			Instantiate (Projectile, SpawnPoint.position, SpawnPoint.rotation);

			}

	}
}

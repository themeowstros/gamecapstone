﻿using UnityEngine;
using System.Collections;

public class GuiManager : MonoBehaviour {
	public Sprite[] sprites;
	SpriteRenderer phaseRenderer;
	SpriteRenderer deflectRenderer;

	// Use this for initialization
	void Start () {
		SpriteRenderer[] meters = gameObject.GetComponentsInChildren<SpriteRenderer>();
		phaseRenderer = meters[0];
		deflectRenderer = meters[1];
	}
	
	// Update both bars
	public void BarsUpdate (float phaseTime, float deflectTime) {
		PhaseUpdate(phaseTime);
		DeflectUpdate(deflectTime);
	}

	// Update the phasebars
	void PhaseUpdate(float phaseTime)
	{
		if(phaseTime<(.2))
		{
			phaseRenderer.sprite = sprites[0];
		}
		else
		if(phaseTime<(.4))
		{
			phaseRenderer.sprite = sprites[1];
		}
		else
		if(phaseTime<(.6))
		{
			phaseRenderer.sprite = sprites[2];
		}
		else
		if(phaseTime<(.8))
		{
			phaseRenderer.sprite = sprites[3];
		}
		else
		if(phaseTime<(1))
		{
			phaseRenderer.sprite = sprites[4];
		}
		else
		{
			phaseRenderer.sprite = sprites[5];
		}
	}

	// update the deflectbars
	void DeflectUpdate(float deflectTime)
	{
		if(deflectTime<(.2))
		{
			deflectRenderer.sprite = sprites[6];
		}
		else
		if(deflectTime<(.4))
		{
			deflectRenderer.sprite = sprites[7];	
		}
		else
		if(deflectTime<(.6))
		{
			deflectRenderer.sprite = sprites[8];	
		}
		else
		if(deflectTime<(.8))
		{
			deflectRenderer.sprite = sprites[9];	
		}
		else
		if(deflectTime<(1))
		{
			deflectRenderer.sprite = sprites[10];
		}
		else
		{
			deflectRenderer.sprite = sprites[11];	
		}
	}
}

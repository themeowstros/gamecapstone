﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialEnd : MonoBehaviour {
	Text text;
	public string prompt;
	GameController gameController;
	public float transitionTime;

	// Use this for initialization
	void Start () {
		text = FindObjectOfType<Text>();
		gameController = Object.FindObjectOfType<GameController>();
	}

	// Player hits the trigger
	void OnTriggerEnter(Collider other)
	{
		if(other.tag.Equals("Player"))
		StartCoroutine(TimedExit());
	}

	IEnumerator TimedExit()
	{
		gameController.Pause();
		gameController.inputLock = true;
		yield return StartCoroutine(WaitForRealSeconds(transitionTime));
		gameController.inputLock = false;
		gameController.Pause();
		Application.LoadLevel("Shop_Level02");
	}
	
	IEnumerator WaitForRealSeconds(float time)
	{
		float start = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup < start + time)
			yield return null;
	}
}

﻿using UnityEngine;
using System.Collections;

public class PlayerControllerTemp : MonoBehaviour {
	
	public float runSpeed;
	public float jumpHeight;
	public float timeToJumpApex;
	public float gravityMultiplier;
	public int maxAirJumps;

	public bool initialPhasedState;
	public float phaseDuration;
	public float phaseCooldown;

	public bool initialShieldState;
	public float shieldDuration;
	public float shieldCooldown;
	
	private Vector2 velocityVector;
	private bool sliding;

	private bool  phased;
	private float phaseTimeLeft;
	private bool  phaseLock;

	private bool  shielded;
	private float shieldTimeLeft;
	private bool  shieldLock;

	private int airJumpsLeft;
	private float jumpVelocity;
	private float jumpingGravity;
	private float fallingGravity;
	private float gravity;
	private bool deathLock;
	
	GameController gameController;
	PlayerPhysics playerPhysics;
	GameObject shield;
	GuiManager guiManager;
	Animator animator;

	//Initialization
	void Start ()
	{
		calculateJumpPhysics();
		animator = GetComponent<Animator>();

		gameController = Object.FindObjectOfType<GameController>();
		playerPhysics = GetComponent<PlayerPhysics>();
		velocityVector = new Vector3(runSpeed, 0.0f, 0.0f);
		guiManager = Object.FindObjectOfType<GuiManager>();
		phaseTimeLeft = 0.0f;
		phased = initialPhasedState;
		
		shieldTimeLeft = 0.0f;
		shielded = initialShieldState;
		shield.SetActive(shielded);
	}

	void Awake () {
		shield = transform.FindChild("DeflectShield").gameObject;
	}

	//Mainly used for projectile collision detection.
	void OnCollisionEnter(Collision other) {
		if(other.transform.tag.Equals("Projectile"))
			if( other.gameObject.GetComponent<ProjectileMover>().dangerous)
			  {
				if(!shielded)
					Die ();
		       else other.gameObject.GetComponent<ProjectileMover>().dangerous = false;
			  }
	}

	void Die() {
		if(!deathLock)
		{
		animator.SetBool(Animator.StringToHash("Die"), true);
		animator.SetBool(Animator.StringToHash("Die"), false);
		gameController.RespawnPlayer();
			deathLock = true;
		}
	}

	void calculateJumpPhysics() {
		jumpVelocity = 2 * jumpHeight / timeToJumpApex;
		jumpingGravity = -2 * (jumpHeight - (jumpVelocity * timeToJumpApex)) / (timeToJumpApex * timeToJumpApex);
		fallingGravity = jumpingGravity * gravityMultiplier;
		//		gravity = 2 * jumpHeight / (timeToJumpApex * timeToJumpApex);
	}

	void PhaseUpdate(float update)
	{
		phaseTimeLeft += update;
		if(phaseTimeLeft<=0.0f)
		{
			phaseTimeLeft = 0;
			phaseLock = true;
		}
		if(phaseTimeLeft >= phaseDuration)
		{
			phaseTimeLeft = phaseDuration;
			phaseLock = false;
		}
	}

	void ShieldUpdate(float update)
	{
		shieldTimeLeft += update;
		if(shieldTimeLeft <= 0.0f)
		{
			shieldTimeLeft = 0;
			shieldLock = true;
		}
		if(shieldTimeLeft >= shieldDuration)
		{
			shieldTimeLeft = shieldDuration;
			shieldLock = false;
		}
	}

	void ChangeAlpha(float alpha)
	{
		Renderer[] renderers = GetComponentsInChildren<Renderer>();
		foreach (var r in renderers)
		{
			if(r.name.Equals("Particle System"))
			   break;
			Color newColor;
			newColor = r.material.color;
			newColor.a = alpha;
			r.material.color = newColor;
		}
	}

	void Update()
	{
		//Reset Slide State so player has to actively hold down slide while on ground
		sliding = false;
		animator.SetBool(Animator.StringToHash("Sliding"), false);

		//Update the phase-state
		//phaseUpdate();
		//shieldUpdate();
		
		//Pause the level
		if(Input.GetButtonDown("Pause"))
		{
			gameController.Pause();
		}
		
		//Restarts the level
		if(Input.GetButtonDown("Restart"))
			gameController.RespawnPlayer();
		
		//Close Game
		if(Input.GetButtonDown("Quit"))
		{
			Application.Quit();
		}

		//If we are not phase locked then we check for phase input. If positive decrease phase time,
		//else increase phase time.
		if(!phaseLock && Input.GetAxis("Phase")>0.0f)
		{
			PhaseUpdate(-Time.deltaTime);
			phased = true;
			ChangeAlpha(0.5f);
		}
		else
		{
			if(phaseTimeLeft!=phaseDuration)
			{
				PhaseUpdate(Time.deltaTime);
			}
			phased = false;
			ChangeAlpha(1.0f);
		}

		//If we are not shield locked then we check for shield input. If positive decrease shield time,
		//else increase shield time.
		if(!shieldLock && Input.GetAxis("Shield")>0.0f)
		{
			ShieldUpdate(-Time.deltaTime);
			shielded = true;
			shield.SetActive(true);
		}
		else
		{
			if(shieldTimeLeft!=shieldDuration)
			{
				ShieldUpdate(Time.deltaTime);
			}
			shielded = false;
			shield.SetActive(false);
		}

		guiManager.BarsUpdate(phaseTimeLeft/phaseDuration,shieldTimeLeft/shieldDuration);

		if(playerPhysics.grounded)
		{
			animator.SetBool(Animator.StringToHash("Jump"), false);
			animator.SetBool(Animator.StringToHash("Grounded"), true);
			velocityVector.y = 0.0f;
			airJumpsLeft = maxAirJumps;
			if(Input.GetButtonDown("Jump"))
			{
				velocityVector.y = jumpVelocity;
				animator.SetBool(Animator.StringToHash("Jump"), true);
				animator.SetBool(Animator.StringToHash("Jump"), false);
			}
			else if (Input.GetAxis("Vertical") < 0.0f) {
				sliding = true;
				animator.SetBool(Animator.StringToHash("Sliding"), true);
			}
		}
		else {
			if(Input.GetButtonDown("Jump") && airJumpsLeft > 0.0f)
			{
				airJumpsLeft--;
				velocityVector.y = jumpVelocity;
			}
		}
		
		if (velocityVector.y >= 0.0f && Input.GetButton("Jump")) {
			gravity = jumpingGravity;
		}
		else {
			gravity = fallingGravity;
		}

		if (velocityVector.y < 0.0f) {
			animator.SetBool(Animator.StringToHash("Falling"), true);
		}
		else {
			animator.SetBool(Animator.StringToHash("Falling"), false);
		}

		velocityVector.x = runSpeed;
		velocityVector.y -= gravity * Time.deltaTime;
		bool safe = playerPhysics.Move(velocityVector * Time.deltaTime, phased, sliding);
		if (!safe) {
			Die ();
		}
		
	}
}

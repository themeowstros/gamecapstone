﻿using UnityEngine;
using System.Collections;

public class BossSpawn : MonoBehaviour {
	public GameObject boss;

	GameController gameController;
	Vector3 SpawnLocation;
	Quaternion SpawnRotation;
	Object bossInstance;

	// Use this for initialization
	void Start () {
		gameController = Object.FindObjectOfType<GameController>();
		SpawnLocation = new Vector3 (330f, 9.10f, -11.6f);
		SpawnRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void OnTriggerEnter(Collider other)
	{
		bossInstance = GameObject.Instantiate(boss,SpawnLocation,SpawnRotation);
	}
}

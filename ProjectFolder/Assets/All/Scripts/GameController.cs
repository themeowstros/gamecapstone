﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public GameObject player;
	public float respawnDelay;
	public float startDelay;
	public 	bool inputLock = false;

	Object playerInstance;
	float timeScale;
	Vector3 SpawnLocation;
	Quaternion SpawnRotation;
	bool destroyLock = false;

	// Use this for initialization
	void Start () {
		timeScale = Time.timeScale;
		SpawnLocation = transform.position;
		SpawnRotation = transform.rotation;
		SpawnPlayer();
		StartCoroutine(TimedPause(startDelay));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Transform PlayerInstance()
	{
		return ((GameObject)playerInstance).transform;
	}

	public void Pause()
	{
	if(inputLock)
			return;
	if(timeScale>0)
		{
			Time.timeScale = 0;
			timeScale = 0;
		}
	else
		{
			Time.timeScale = 1;
			timeScale = 1;
		}

	}

	public void CheckPoint(Vector3 location)
	{
		SpawnLocation = location;
	}

	public void SpawnPlayer()
	{
		playerInstance = GameObject.Instantiate(player,SpawnLocation,SpawnRotation);
	}

	public void KillPlayer()
	{
		Destroy(playerInstance);
	}

	public void RespawnPlayer()
	{
		if(destroyLock)
			return;
		destroyLock = true;
		if(timeScale==0)
			Application.LoadLevel(Application.loadedLevel);
		KillPlayer();
		SpawnPlayer();
		StartCoroutine(TimedPause(respawnDelay));
		destroyLock = false;
	}

	IEnumerator TimedPause(float time)
	{
		Pause();
		inputLock = true;
		yield return StartCoroutine(WaitForRealSeconds(time));
		inputLock = false;
		Pause();
	}

	IEnumerator WaitForRealSeconds(float time)
	{
		float start = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup < start + time)
			yield return null;
	}
}

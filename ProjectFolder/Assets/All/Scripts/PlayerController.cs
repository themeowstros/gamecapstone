﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;
	public float jumpHeight;
	public float gravity;
	public float phaseDuration;
	public float phaseCooldown;
	public bool initialPhasedState;

	private Vector2 amountToMove;
	private bool phased;
	private bool sliding;
	private float phaseTimeLeft;
	private float phaseCooldownLeft;

	PlayerPhysics playerPhysics;

	//Initialization
	void Start ()
	{
		playerPhysics = GetComponent<PlayerPhysics>();
		amountToMove = new Vector3(speed,0,0);
		phaseCooldownLeft = 0.0f;
		phaseTimeLeft = 0.0f;
		phased = initialPhasedState;
	}

	void FixedUpdate () {
		if (phaseTimeLeft > 0) {
			phaseTimeLeft = Mathf.Max(phaseTimeLeft - Time.fixedDeltaTime, 0);
			if (phaseTimeLeft == 0) {
				phased = false;
				phaseCooldownLeft = phaseCooldown;
			}
		}
		else if (phaseCooldownLeft > 0) {
			phaseCooldownLeft = Mathf.Max(phaseCooldownLeft - Time.fixedDeltaTime, 0);
		}
	}

	void Update()
	{
		//Restarts the level
		if(Input.GetButtonDown("Restart"))
			Application.LoadLevel(0);

		//Close Game
		if(Input.GetButtonDown("Quit"))
		{
			Debug.Log ("I quit");
			Application.Quit();
		}

		if (!phased && phaseCooldownLeft == 0) {
			phased = Input.GetButtonDown("Fire1");
			phaseTimeLeft = phaseDuration;
		}
		Debug.Log ("Phased: " + phased);

		if(playerPhysics.grounded)
		{
			amountToMove.y = 0;
			if(Input.GetButtonDown("Jump"))
			{
				Debug.Log("I jumped");
				amountToMove.y = jumpHeight;
			}
			else if (Input.GetAxis("Vertical") < 0) {
				Debug.Log("I slid");
				sliding = true;
			}
		}
		else
			Debug.Log("NOT GROUNDED!");
		amountToMove.x = speed;
		amountToMove.y -= gravity * Time.deltaTime;
		bool safe = playerPhysics.Move(amountToMove * Time.deltaTime, phased, sliding);
		if (!safe) {
			Application.LoadLevel(0);
		}
	}
}

﻿using UnityEngine;
using System.Collections;


public class BossScript : MonoBehaviour {
	public float smoothTime = .6f;
	public Transform target;
	public Vector3 offset;

	GameController gameController;

	private Vector3 velocity = Vector3.zero;

	// Use this for initialization
	void Start () {
		gameController = Object.FindObjectOfType<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
		if(target == null)
		{
			target = gameController.PlayerInstance();
		}
		else
		{
			Vector3 targetPosition = target.position;
			transform.position = Vector3.SmoothDamp(transform.position, targetPosition+offset, ref velocity, smoothTime);
		}
	}
}

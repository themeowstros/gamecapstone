﻿using UnityEngine;
using System.Collections;

public class ProjectileMover : MonoBehaviour {
	
	public float speed;
	public float life;
	public float collisionDeathTime;
	private float start;
	public bool dangerous;
	
	void Start ()
	{
		dangerous = true;
		rigidbody.velocity = transform.right * speed;
		start = Time.time;
		
	}
	
	void OnCollisionEnter (Collision other) {
		start = Time.time;
		life = collisionDeathTime;
	}
	
	void Kill () {
		Destroy(gameObject);
	}
	
	void Update ()
	{
		if (Time.time - start > life)
		{
			Debug.Log(life);
		 Destroy (gameObject);
		}
//		Debug.Log (Time.time);
		
	}
}

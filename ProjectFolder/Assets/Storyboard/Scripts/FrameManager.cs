﻿using UnityEngine;
using System.Collections;

public class FrameManager : MonoBehaviour {
	public Sprite[] sprites;
	public SpriteRenderer frameRend;
	float currTime = 0f;
	
	// Use this for initialization
	void Start () {
		SpriteRenderer[] frame = gameObject.GetComponentsInChildren<SpriteRenderer>();
		frameRend = frame[0];
	}

	void FixedUpdate() {
		currTime += Time.deltaTime;
		FrameUpdate (currTime);
	}
	
	// Update both bars
	public void FrameUpdate (float t) {
		if (t == 0) {
			frameRend.sprite = sprites[0];
		}
		if (t > 2) {
			frameRend.sprite = sprites[1];	
		}
	}
}
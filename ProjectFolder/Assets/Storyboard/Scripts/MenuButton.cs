﻿using UnityEngine;
using System.Collections;

public class MenuButton : MonoBehaviour {
	
	public Color default_color;
	public Color enter_color;
	public bool isMenu;
	public GameObject camera;

	TextMesh textmesh;

	void Start() 
	{
		textmesh = GetComponent<TextMesh>();
		textmesh.color = default_color;
	}
	
	void OnMouseEnter(){ textmesh.color = enter_color; }
	void OnMouseExit() { textmesh.color = default_color; }
	void OnMouseDown() { 
		if(isMenu) {
			Application.LoadLevel(0); /* Replace with string of level name */
		}
	}
	
}

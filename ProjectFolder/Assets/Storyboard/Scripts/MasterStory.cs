﻿using UnityEngine;
using System.Collections;

public class MasterStory : MonoBehaviour {
	public Animator[] anims;
	public int currentAnim;
	public bool playNext = false;

	// Use this for initialization
	void Start () {
		anims = this.GetComponentsInChildren<Animator> ();
		currentAnim = 0;
	}
	
	void Update () 
	{
		if(currentAnim > anims.Length) { }
		else {
				//get the current state
			AnimatorStateInfo stateInfo = anims[currentAnim].GetCurrentAnimatorStateInfo(0);

			Debug.Log ("currentAnim:" + currentAnim);
			if(stateInfo.nameHash == Animator.StringToHash("Base.DoneAnim") && !playNext) {
				Debug.Log("YES");

				anims[currentAnim].Play("Base.StartAnim");
				playNext = true;
			}
			int currState = stateInfo.nameHash;


			if(playNext && isEndState(currState)) {
	//			AnimatorStateInfo animInfo = anims[currentAnim].GetNextAnimatorStateInfo(0);
	//			Debug.Log ("Length of anim: " + animInfo.length * animInfo.normalizedTime);
	//			float delta = Time.time;
	//			while(Time.time - delta < 2) {Debug.Log ("Waiting");}
				//Time.time - delta < 2
				playNext = false;
				currentAnim++;
			} 
		} 
	}

	bool isEndState(int hash) {
		int[] hashEndStates = {Animator.StringToHash ("Base.Frame2Done"), Animator.StringToHash ("Base.Frame3Done")};
		for(int i = 0; i < hashEndStates.Length; i++) {
			if(hash == hashEndStates[i]) {
				return true;
			}
		}
		return false;
	}
}
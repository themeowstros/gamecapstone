﻿using UnityEngine;
using System.Collections;

public class RotateClouds : MonoBehaviour {

	public float scrollSpeed = 0.5F;

	// Update is called once per frame
	void Update () {
		float offset = Time.time * scrollSpeed;
		Vector2 offsetVector = new Vector2 (offset, 0);
		renderer.material.SetTextureOffset("_MainTex", offsetVector);
	}
}

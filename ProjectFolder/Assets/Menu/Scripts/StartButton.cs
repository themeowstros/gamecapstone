﻿using UnityEngine;
using System.Collections;

public class StartButton : MonoBehaviour {
	
	public Color default_color;
	public Color enter_color;
	public bool isStart;
	public GameObject camera;
	public GameObject menuPlayer;
	private Animator charAnim;

	TextMesh textmesh;

	void Start() {
		textmesh = GetComponent<TextMesh>();
		textmesh.color = default_color;
	}
	
	void OnMouseEnter(){ textmesh.color = enter_color; }
	void OnMouseExit() { textmesh.color = default_color; }
	void OnMouseDown() { 
		if(isStart) {
			charAnim = menuPlayer.GetComponent<Animator>();
			charAnim.SetTrigger("Move");
			camera.animation.Play("StartAnim");
			charAnim.SetBool("Jump", true);

			GetComponent<TextMesh>().renderer.enabled = false;
		}
	}

	void Update() {
		if(charAnim != null) {
			AnimatorStateInfo asi = charAnim.GetCurrentAnimatorStateInfo(0);
			if(asi.nameHash == Animator.StringToHash("Movement.Idle") && 
			   !charAnim.IsInTransition(0)) {
				charAnim.SetBool("Jump", false);
				Debug.Log ("Idling");
			}
			else {
				charAnim.SetBool ("Jump", true);
			}
		}
	}
	
}

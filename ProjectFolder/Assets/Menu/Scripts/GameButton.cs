﻿using UnityEngine;
using System.Collections;

public class GameButton : MonoBehaviour {
	
	public Color defaultColor;
	public Color enterColor;
	public GameObject board;
	public GameObject introBoard;

	TextMesh textmesh;

	void Start() 
	{
		board.SetActive (false);
		textmesh = GetComponent<TextMesh>();
		textmesh.color = defaultColor;
	}

	void OnMouseEnter() { textmesh.color = enterColor; }
	void OnMouseExit() { textmesh.color = defaultColor; }
	void OnMouseDown() { 
		introBoard.SetActive (false);
		board.SetActive (true);
	}
}

﻿using UnityEngine;
using System.Collections;

public class StoryButton : MonoBehaviour {
	
	public Color defaultColor;
	public Color enterColor;

	TextMesh textmesh;

	void Start() {
		textmesh = GetComponent<TextMesh>();
		textmesh.color = defaultColor;
	}
	
	void OnMouseEnter() { textmesh.color = enterColor; }
	void OnMouseExit() { textmesh.color = defaultColor; }
	void OnMouseDown() { 
		Application.LoadLevel (1); /* Replace with string of level name */
	}
}
